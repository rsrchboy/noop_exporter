                                                          _
     _ __   ___   ___  _ __     _____  ___ __   ___  _ __| |_ ___ _ __
    | '_ \ / _ \ / _ \| '_ \   / _ \ \/ / '_ \ / _ \| '__| __/ _ \ '__|
    | | | | (_) | (_) | |_) | |  __/>  <| |_) | (_) | |  | ||  __/ |
    |_| |_|\___/ \___/| .__/___\___/_/\_\ .__/ \___/|_|   \__\___|_|
                    |_| |_____|       |_|

This is a trivially tiny exporter that only exports some basic, default
metrics.  It is likely to only be useful in testing or development situations
where you just need _something_ to reply with valid metrics.

# LICENSE

Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
