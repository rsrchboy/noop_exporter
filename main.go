/*
Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
                                                       _
  _ __   ___   ___  _ __     _____  ___ __   ___  _ __| |_ ___ _ __
 | '_ \ / _ \ / _ \| '_ \   / _ \ \/ / '_ \ / _ \| '__| __/ _ \ '__|
 | | | | (_) | (_) | |_) | |  __/>  <| |_) | (_) | |  | ||  __/ |
 |_| |_|\___/ \___/| .__/___\___/_/\_\ .__/ \___/|_|   \__\___|_|
                   |_| |_____|       |_|

This is a trivially tiny exporter that only exports some basic, default
metrics.  It is likely to only be useful in testing or development situations.

*/
package main

import (
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const bindAddress = ":8080"

func main() {
	// pretty logging!
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	prometheus.MustRegister(collectors.NewBuildInfoCollector())
	http.Handle("/metrics", promhttp.Handler())

	log.Info().Str("listening-at", bindAddress).Msg("starting the no-op collector")
	if err := http.ListenAndServe(bindAddress, nil); err != nil {
		log.Fatal().Err(err).Msg("http.ListenAndServe() bailed!")
	}
}
