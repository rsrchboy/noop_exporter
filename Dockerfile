FROM docker.io/library/golang:1.16-alpine3.14 as builder

ADD . /our-src
WORKDIR /our-src
RUN go mod download
RUN go build -o /noop_exporter

FROM docker.io/library/alpine:3.14

COPY --from=builder --chown=0:0 /noop_exporter /noop_exporter

ENTRYPOINT ["/noop_exporter"]
